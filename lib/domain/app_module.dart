import 'package:dartin/dartin.dart';
import 'package:dio/dio.dart';
import 'package:wfm_check/values/const/api_const.dart';
import '../utils/shared_preferences.dart';

const testScope = DartInScope('wfm');

class AuthInterceptor extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    final token = spUtil.getString(API_TEST_TOKEN);
    final auth = "Basic Yml6bmV0cG9ydGFsOmIxem4zdDIwMThwMHJ0NGw=";
    final contentType = "application/json";
    //final token = null;
    options.headers.update("Authorization", (_) => auth, ifAbsent: () => auth);
    options.headers.update("api-token", (_) => token, ifAbsent: () => token);
    // options.headers
    //     .update("Content-Type", (_) => auth, ifAbsent: () => contentType);
    super.onRequest(options, handler);
  }
}

final dio = Dio()
  ..options = BaseOptions(
      connectTimeout: 30000,
      receiveTimeout: 30000)
  ..interceptors.add(AuthInterceptor())
  ..interceptors.add(LogInterceptor(responseBody: true, requestBody: true));

SpUtil spUtil;

init() async {
  spUtil = await SpUtil.getInstance();
}
