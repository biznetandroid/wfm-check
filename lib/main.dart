import 'package:bloc/bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:wfm_check/ui/home/home_screen.dart';
import 'package:wfm_check/ui/testapi/test_api_screen.dart';
import 'package:wfm_check/ui/workorder/search_engineer_screen.dart';
import 'package:wfm_check/ui/workorder/search_workorder_screen.dart';
import 'package:wfm_check/ui/workorder/task_workorder_screen.dart';
import 'package:wfm_check/utils/simple_bloc_delegate.dart';

import 'domain/app_module.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await init();
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire:
        builder: (context, snapshot) {
          return MaterialApp(
              title: 'WFM Check',
              theme: ThemeData(fontFamily: 'Poppins'),
              home: HomeScreen(),
              routes: {
                '/testApiScreen': (context) => TestAPIScreen(),
                '/searchEngineerScreen': (context) => SearchEngineerScreen(),
                '/searchWorkOrderScreen': (context) => SearchWorkOrderScreen(),
                '/taskWorkOrderScreen': (context) => TaskWorkOrderScreen()
              }
          );
        }
    );
  }
}

