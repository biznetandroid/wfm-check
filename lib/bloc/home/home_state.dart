part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();
}

class HomeInitialState extends HomeState {

  @override
  List<Object> get props => null;
}

class HomeLoadingState extends HomeState {

  @override
  List<Object> get props => null;
}

class SearchEngineerSuccessState extends HomeState {
  List<Engineer> data;

  SearchEngineerSuccessState({this.data});

  @override
  List<Object> get props => [data];
}

class GetTaskSuccessState extends HomeState {
  List<Task> data;

  GetTaskSuccessState({this.data});

  @override
  List<Object> get props => [data];
}

class CheklistCompleteState extends HomeState {
  bool success;

  CheklistCompleteState({this.success});

  @override
  List<Object> get props => [success];
}

class InjectChatComplete extends HomeState {


  @override
  List<Object> get props => null;
}
