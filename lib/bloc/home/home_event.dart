part of 'home_bloc.dart';



abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class InitialHomeEvent extends HomeEvent {
  String userId;
  String workId;
  InitialHomeEvent({this.userId, this.workId});
  @override
  List<Object> get props => [userId, workId];

}

class SearchEngineerEvent extends HomeEvent {
  String keyword;

  SearchEngineerEvent({this.keyword});

  @override
  List<Object> get props => [keyword];

}

class ChecklistEvent extends HomeEvent {
  Task task;
  String userId;

  ChecklistEvent({this.userId, this.task});

  @override
  List<Object> get props => [userId, task];

}

class UnchecklistEvent extends HomeEvent {
  Task task;
  String userId;

  UnchecklistEvent({this.userId, this.task});

  @override
  List<Object> get props => [userId, task];

}
class InjectChatEvent extends HomeEvent {


  @override
  List<Object> get props => null;

}