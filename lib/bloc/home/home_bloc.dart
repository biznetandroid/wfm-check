import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:wfm_check/data/engineer.dart';
import 'package:wfm_check/data/repository/firestore_repository.dart';
import 'package:wfm_check/data/task.dart';

part 'home_event.dart';
part 'home_state.dart';


class HomeBloc extends Bloc<HomeEvent, HomeState> {

  final databaseReference  = FirebaseFirestore.instance;
  FirestoreRepository _repository;

  HomeBloc({FirestoreRepository repository}) : assert(repository != null),
        _repository = repository;

  @override
  HomeState get initialState => HomeInitialState();

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if(event is InitialHomeEvent){
      List<Task> tasks;
      await databaseReference
          .collection('User')
          .doc(event.userId)
          .collection("WorkOrder")
          .doc(event.workId)
          .collection("TaskDone")
          .get().then((QuerySnapshot querySnapshot) {
              print("size task "+querySnapshot.size.toString());
              tasks =querySnapshot.docs.map((doc) => Task.fromDocument(doc)).toList().cast<Task>();
          });
      yield GetTaskSuccessState(data: tasks);

    }else if(event is ChecklistEvent){
      yield HomeLoadingState();
      bool checked = false;

      await databaseReference
          .collection('User')
          .doc(event.userId)
          .collection("WorkOrder")
          .doc(event.task.workId)
          .collection("TaskDone")
          .doc().set(event.task.toJson())
          .then((value) => {
            checked = true
          })
          .catchError((error) => print("Failed to checklist: $error"));

      yield CheklistCompleteState(success: checked);
    }else if(event is UnchecklistEvent){
      yield HomeLoadingState();
      bool checked = false;

      final references = databaseReference
          .collection('User')
          .doc(event.userId)
          .collection("WorkOrder")
          .doc(event.task.workId)
          .collection("TaskDone");

      await references
          .where("name", isEqualTo: event.task.name)
          .get()
          .then((QuerySnapshot querySnapshot) => {
            querySnapshot.docs.forEach((doc) {
              references.doc(doc.id).delete().then((value) => {checked = true})
                  .catchError((error) => print("Failed to checklist: $error"));
            })
           })
          .catchError((error) => print("Failed to checklist: $error"));

      yield CheklistCompleteState(success: checked);
    }
    else if(event is InjectChatEvent){

      yield HomeLoadingState();

      final references = databaseReference
          .collection('chat');

      final today = DateTime.now();
      final date = DateTime(today.year,today.month, today.day-3);

      print("ord ${date.day} ${date.month} ${date.year}");
      await references
          .where("created", isGreaterThan: date)
          .get()
          .then((QuerySnapshot querySnapshot) => {
            querySnapshot.docs.forEach((doc) {
              print("ord ${doc.id}");
              references.doc(doc.id).collection("messages").doc().delete().catchError((error) => print("Failed to delete: $error"));
              references.doc(doc.id).delete().catchError((error) => print("Failed to delete: $error"));
              // references.doc(doc.id).collection("messages").doc()
              //     .set({
              //   'user_id': "0",
              //   'name': "",
              //   'created': Timestamp.now(),
              //   'message': ""
              // })
              // .catchError((error) => print("Failed to inject: $error"));
        })
      })
          .catchError((error) => print("Failed to checklist: $error"));

      yield InjectChatComplete();
    }
  }
}
