part of 'test_api_bloc.dart';



abstract class TestAPIEvent extends Equatable {
  const TestAPIEvent();
}

class InitialTestAPIEvent extends TestAPIEvent {

  @override
  List<Object> get props => null;

}

class TestApiPostEvent extends TestAPIEvent {

  String url;
  Map data;
  TestApiPostEvent({this.url, this.data});

  @override
  List<Object> get props => [url, data];

}

class TestApiGetEvent extends TestAPIEvent {

  String url;
  TestApiGetEvent({this.url});

  @override
  List<Object> get props => [url];

}
