import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:wfm_check/data/engineer.dart';
import 'package:wfm_check/data/repository/firestore_repository.dart';
import 'package:wfm_check/data/repository/test_api_repository.dart';
import 'package:wfm_check/data/task.dart';

part 'test_api_event.dart';
part 'test_api_state.dart';


class TestAPIBloc extends Bloc<TestAPIEvent, TestAPIState> {

  final databaseReference  = FirebaseFirestore.instance;
  TestApiRepository _repository;

  TestAPIBloc({TestApiRepository repository}) : assert(repository != null),
        _repository = repository;

  @override
  TestAPIState get initialState => TestAPIInitialState();

  @override
  Stream<TestAPIState> mapEventToState(
    TestAPIEvent event,
  ) async* {
    if(event is InitialTestAPIEvent){

    }else if(event is TestApiPostEvent){
      yield TestAPILoadingState(true);

      Response response = await _repository.sendPost(event.url, event.data);
      yield TestAPILoadingState(false);
      if(response.statusCode == 200){
        yield TestApiSuccessState(message: json.encode(response.data), code: response.statusCode);
      }else{
        yield TestApiFaildState(message: json.encode(response.data), code: response.statusCode);
      }
    }else if(event is TestApiGetEvent){
      yield TestAPILoadingState(true);

      Response response = await _repository.sendGet(event.url);
      yield TestAPILoadingState(false);
      if(response.statusCode == 200){
        yield TestApiSuccessState(message: response.data, code: response.statusCode);
      }else{
        yield TestApiFaildState(message: response.data, code: response.statusCode);
      }
    }
  }
}
