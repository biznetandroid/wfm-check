part of 'test_api_bloc.dart';

abstract class TestAPIState extends Equatable {
  const TestAPIState();
}

class TestAPIInitialState extends TestAPIState {

  @override
  List<Object> get props => null;
}

class TestAPILoadingState extends TestAPIState {
  bool show;
  TestAPILoadingState(this.show);

  @override
  List<Object> get props => [show];
}

class TestApiSuccessState extends TestAPIState {

  int code;
  String message;
  TestApiSuccessState({this.message, this.code});

  @override
  List<Object> get props => [message, code];
}

class TestApiFaildState extends TestAPIState {

  int code;
  String message;
  TestApiFaildState({this.message, this.code});

  @override
  List<Object> get props => [message, code];
}

