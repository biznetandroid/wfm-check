
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:wfm_check/bloc/home/home_bloc.dart';
import 'package:wfm_check/data/engineer.dart';
import 'package:wfm_check/data/repository/firestore_repository.dart';
import 'package:wfm_check/values/app_colors.dart';

class HomeScreen extends StatefulWidget
{

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final databaseReference  = FirebaseFirestore.instance;
  final keywordController = TextEditingController();
  List<Engineer> items = List<Engineer>();
  bool isLoading = false;
  String keyword = "";
  String fieldName = "Username";
  String fieldValue = "username";
  final loadingDialog = Dialog(
    child: new Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        new CircularProgressIndicator(),
        new Text("Loading"),
      ],
    ),
  );

  FirestoreRepository repository;



  @override
  void dispose() {
    keywordController.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    repository = FirestoreRepository();
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        title: Text("WFM Check", style: TextStyle(color: AppColors.darkTextColor, fontWeight: FontWeight.bold),),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: BlocProvider(
        create: (context) => HomeBloc(repository: FirestoreRepository()),
        child:
        BlocListener<HomeBloc, HomeState>(
            listener: (context, state) {
              if(state is HomeLoadingState){
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return loadingDialog;
                  },
                );
              }else if(state is SearchEngineerSuccessState){
                setState(() {
                  items = state.data;
                });
              }else if(state is InjectChatComplete){
                Navigator.pop(context);
              }
            },
            child: BlocBuilder<HomeBloc, HomeState>(
              builder: (context, state) {
                return buildContent(context, state);
              },
            )),
      )
       // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget buildNoResult() => Container(
    width: double.infinity,
    padding: EdgeInsets.only(top:64),
    child: SingleChildScrollView(child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SvgPicture.asset("assets/images/no_data.svg", width: 150, height: 150,),
        Padding(
          padding: EdgeInsets.only(top: 62, bottom: 8),
          child: Text("Oops .. ", style: TextStyle(color: AppColors.textInactive.withAlpha(120),  fontSize: 20, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
        ),
        Text("User tidak ditemukan", style: TextStyle(color: AppColors.textInactive.withAlpha(120),  fontSize: 16), textAlign: TextAlign.center,),
      ],
    ),),
  );

  Widget buildContent(BuildContext context, HomeState state) {
    final widthWidget = MediaQuery.of(context).size.width - 64;
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        children: [
          buildItemList("Work Order Firebase", "Check active work order and tasks based on firebase", "fire.png", () {
            Navigator.of(context).pushNamed("/searchEngineerScreen");
          }),
          buildItemList("Test API", "Test some API WFM production & development ", "test.png", () {
            Navigator.of(context).pushNamed("/testApiScreen");
          }),

          buildItemList("Clear Chat", "Clear chat so the work order can be done well ", "inject.png", () {
            BlocProvider.of<HomeBloc>(context).add(
               InjectChatEvent()
            );
          })
        ],
      )
    );
  }

  Widget buildItemList(String title, String subtitle, String icon, Function onTap) =>  Container(
      clipBehavior: Clip.hardEdge,
      margin: EdgeInsets.fromLTRB(0, 16, 0, 0),
      decoration: BoxDecoration(
        color: AppColors.greyLight80.withOpacity(0.3),
        borderRadius: BorderRadius.all(Radius.circular(20)),

      ),
        child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: onTap,
              child: Padding(
                padding: EdgeInsets.fromLTRB(24, 16, 24, 16),

                child: Row(
                  children: [

                    Expanded(child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(title, style: TextStyle(fontWeight: FontWeight.bold, color: AppColors.darkTextColor, fontSize: 20),),
                        Text(subtitle, style: TextStyle(color: AppColors.grey, fontSize: 12),),

                      ],
                    ),),
                    Container(
                      width: 80,
                      height: 80,
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(left: 16),
                      padding: EdgeInsets.only(left: 4, bottom: 4),
                      decoration: BoxDecoration(
                        color: AppColors.white.withAlpha(100),
                        borderRadius: BorderRadius.all(Radius.circular(18)),

                      ),
                      child: Image.asset("assets/images/$icon",  width: 30, height: 30,),
                    ),
                  ],
                ),
              ),
            )
        )
    );
}