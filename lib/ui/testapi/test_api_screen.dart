
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wfm_check/bloc/testapi/test_api_bloc.dart';
import 'package:wfm_check/data/api_doc.dart';
import 'package:wfm_check/data/engineer.dart';
import 'package:wfm_check/data/repository/firestore_repository.dart';
import 'package:wfm_check/data/repository/test_api_repository.dart';
import 'package:wfm_check/values/app_colors.dart';

class TestAPIScreen extends StatefulWidget
{

  @override
  _TestAPIScreenState createState() => _TestAPIScreenState();
}

class _TestAPIScreenState extends State<TestAPIScreen> {
  final databaseReference  = FirebaseFirestore.instance;
  final urlController = TextEditingController();
  List<Engineer> items = List<Engineer>();
  bool isLoading = false;
  String keyword = "";
  String Re = "";
  String environment = "DEV";
  int tabActive = 0;
  List<ApiDoc> apiList = List<ApiDoc>.empty(growable: true);
  List<TextEditingController> reqControllers = List<TextEditingController>.empty(growable: true);
  String baseUrlPrd = "https://api.biznetnetworks.com";
  String baseUrlDev = "http://api-dev.biznetnetworks.com";

  FirestoreRepository repository;




  @override
  void dispose() {
    urlController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    repository = FirestoreRepository();
    apiList.add(
        ApiDoc(
            name: "Login",
            method: "POST",
            url: "/wfm/v1/loginwfm",
            request: ["username","password","device_token"]
        )
    );

    apiList.add(
        ApiDoc(
            name: "Search Work Order",
            method: "POST",
            url: "/wfm/v1/searchwfm",
            request: ["individualId","workOrderTypeId"]
        )
    );

    apiList.add(
        ApiDoc(
            name: "Detail Work Order",
            method: "POST",
            url: "/wfm/v1/detailworkorder",
            request: ["individualId","workId"]
        )
    );

    setActiveValue();

    super.initState();
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("Test API", style: TextStyle(color: AppColors.darkTextColor, fontWeight: FontWeight.bold),),
        backgroundColor: Colors.white,
        titleSpacing: 0,
        elevation: 0,
        iconTheme: IconThemeData(
          color: AppColors.darkTextColor, //change your color here
        ),
        actions: [
          Container(
            width: 50,
            height: 30,
            margin: const EdgeInsets.only(right: 16, top: 14, bottom: 14),
            child: MaterialButton(
              padding: EdgeInsets.only(left: 8, right: 8),
              elevation: 0,
              color:  environment == "PRD" ? AppColors.red : AppColors.greyLight80,
              child: Text(environment, style: TextStyle(color: environment == "PRD" ? AppColors.white : Colors.black, fontSize: 10, fontWeight: FontWeight.bold),),
              textColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18),
              ),
              onPressed: () {
                setState(() {
                  environment = environment == "PRD" ? "DEV" : "PRD";
                });

              },
            ),
          ),

        ],
      ),
      body: BlocProvider(
        create: (context) => TestAPIBloc(repository: TestApiRepository()),
        child:
        BlocListener<TestAPIBloc, TestAPIState>(
            listener: (context, state) {
              
            },
            child: BlocBuilder<TestAPIBloc, TestAPIState>(
              builder: (context, state) {
                return buildContent(context, state);
              },
            )),
      )
       // This trailing comma makes auto-formatting nicer for build methods.
    );
  }


  Widget buildContent(BuildContext context, TestAPIState state) {
    final widthWidget = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      child: Container(
          // height: MediaQuery.of(context).size.height - kToolbarHeight,
          color: AppColors.greyDefault,
          child: Column(
            children: [

              Container(
                height: 60,
                color: Colors.white.withOpacity(0.8),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: apiList.length,
                  itemBuilder: (context, index) {
                    return buildTab(context, index);
                  },
                ),
              ),

              Container(
                margin: EdgeInsets.only(left: 24, right: 24, top: 24),
                child: Row(
                  children: [
                    Container(
                      height: 40,
                      padding: const EdgeInsets.only(left: 18, right: 12),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.blue.withOpacity(0.2),

                        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(28), topLeft: Radius.circular(28)),
                      ),
                      child: Text(apiList[tabActive].method, style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),),
                    ),

                    Flexible(child: Container(
                      height: 40,
                      padding: const EdgeInsets.only(left: 4,right: 4, bottom: 4),
                      decoration: BoxDecoration(
                        color: AppColors.greyLight80,


                        borderRadius: BorderRadius.only(bottomRight: Radius.circular(28), topRight: Radius.circular(28)),
                      ),
                      child: TextFormField(
                        autocorrect: false,
                        autofocus: false,
                        maxLines: 1,
                        minLines: 1,
                        textInputAction: TextInputAction.send,
                        controller: urlController,

                        decoration: InputDecoration(
                          isDense: true,
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          hintStyle: TextStyle(color: AppColors.greyText, fontSize: 12),
                          hintText: "Input url . .",

                        ),
                      ),
                    ),),
                  ],
                ),
              ),

              Container(
                  margin : EdgeInsets.only(left: 24, right: 24, top: 16),
                  padding : EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 12),
                  decoration: BoxDecoration(
                    color: AppColors.greyLight80,
                    borderRadius: BorderRadius.circular(18),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(bottom: 8),
                        child: Text("Params", style: TextStyle(color: AppColors.textInactive),),
                      ),


                      ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: apiList[tabActive].request.length,
                        itemBuilder: (context, index) {
                          return TextFormField(
                            autocorrect: false,
                            autofocus: false,
                            maxLines: 1,
                            minLines: 1,
                            textInputAction: TextInputAction.send,
                            controller: reqControllers[index],
                            decoration: InputDecoration(
                              isDense: true,
                              enabledBorder: UnderlineInputBorder(
                                  borderSide:  BorderSide(color: AppColors.grey)
                              ),
                              hintStyle: TextStyle(color: AppColors.greyText, fontSize: 12),
                              hintText: apiList[tabActive].request[index],

                            ),
                          );
                        },
                      ),
                    ],
                  )
              ),

              Padding(
                  padding: EdgeInsets.only(left: 24, right: 24, top: 16),
                child:  Row(
                  children: [
                    Spacer(),
                    InkWell(
                      borderRadius: BorderRadius.circular(20),
                      onTap: state is TestAPILoadingState && state.show? null : (){
                        setState(() {
                          String url = urlController.text;
                          // Map reqList;

                          List<String> reqValue = List<String>.empty(growable: true);
                          for(int i=0; i<apiList[tabActive].request.length; i++){
                            reqValue.add(reqControllers[i].text);
                          }
                          Map<String, String> reqList = Map.fromIterables(apiList[tabActive].request, reqValue);

                          BlocProvider.of<TestAPIBloc>(context).add(
                              apiList[tabActive].method == "POST" ? TestApiPostEvent(url: url, data: reqList): TestApiGetEvent(url:  url)
                          );

                        });
                      },
                      child: AnimatedContainer(
                          width: (state is TestAPILoadingState && state.show) ? 45 :100,
                          height: 45,
                          alignment: Alignment.center,
                          curve: Curves.easeInOutCubic,
                          padding:  EdgeInsets.fromLTRB(10, 10, 10, 10),
                          decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius:  BorderRadius.circular(28),
                          ),
                          duration: Duration(milliseconds: 400),
                          child: state is TestAPILoadingState && state.show
                              ? CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation<Color>(
                                Colors.white),
                          )
                              : Text(
                            "Send",
                            style: TextStyle(
                                fontSize: 14,
                                color: Colors.white
                            ),
                          )),
                    )
                  ],
                ),
              ),
              // Align(
              //   alignment: Alignment.centerRight,
              //   child: ,
              // ),

              Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 32, left: 24, right:24, bottom: 24),
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(18),
                ),
                child:  state is TestApiSuccessState ? Text(getPrettyJSONString(state.message), style: TextStyle(fontSize:  12),)
                    : state is TestApiFaildState ? Text(getPrettyJSONString(state.message))
                    : Text(""),
              ),



            ],
          )
      ),
    );
  }

  Widget buildTab(BuildContext context, int index) {
    final widthTab = MediaQuery.of(context).size.width/3;
    return InkWell(
        child: Container(
            width: widthTab,
            height: 60,
          alignment: Alignment.center,
          padding: EdgeInsets.only(left: 16, right: 16),
          child: SizedBox(
              width: widthTab,
              height: 60,
              child: Stack(
            children: [
              Positioned.fill(

                child: Align(
                    alignment: Alignment.center,
                    child: Text(apiList[index].name,textAlign: TextAlign.center, style: TextStyle(fontSize: 12, fontWeight: tabActive == index ? FontWeight.bold: FontWeight.normal, color: tabActive == index ? Colors.blue : AppColors.textInactive),),
                ),
              ),

              tabActive == index ? Positioned(
                bottom: -4,
                  left: 0,
                  right: 0,
                  child: Container(
                width: 8,
                height: 8,

                alignment: Alignment.center,
                decoration:  BoxDecoration(
                  shape: BoxShape.circle,
                  // borderRadius: new BorderRadius.circular(6.0),
                  color: AppColors.greyDefault,
                ),
              )
              ): Text("")
            ],
          )
          ),
        ),
        onTap: (){
          setState(() {
            tabActive = index;
            setActiveValue();
          });

        },
    );
  }

  void setActiveValue(){
    urlController.text = environment=="DEV" ? baseUrlDev+apiList[tabActive].url : baseUrlPrd+apiList[tabActive].url;


    setState(() {
      reqControllers.clear();
      for(int i=0; i<apiList[tabActive].request.length; i++){
        reqControllers.add(TextEditingController());
      }

    });

  }

  String getPrettyJSONString(jsonObject) {
    JsonEncoder encoder = new JsonEncoder.withIndent('  ');
    String jsonString = encoder.convert(json.decode(jsonObject));
    return jsonString;
  }

  // String formatString(String text){
  //
  //   String json = StringB;
  //   String indentString = "";
  //
  //   for (int i = 0; i < text.length; i++) {
  //     final letter = text[i];
  //     switch (letter) {
  //       case '{':
  //       case '[':
  //         json.append("\n" + indentString + letter + "\n");
  //         indentString = indentString + "\t";
  //         json.append(indentString);
  //         break;
  //       case '}':
  //       case ']':
  //         indentString = indentString.replaceFirst("\t", "");
  //         json.append("\n" + indentString + letter);
  //         break;
  //       case ',':
  //         json.append(letter + "\n" + indentString);
  //         break;
  //
  //       default:
  //         json.append(letter);
  //         break;
  //     }
  //   }
  //
  //   return json.toString();
  // }

}