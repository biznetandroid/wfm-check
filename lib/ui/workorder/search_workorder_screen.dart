

import 'package:clipboard/clipboard.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:toast/toast.dart';
import 'package:wfm_check/bloc/home/home_bloc.dart';
import 'package:wfm_check/data/engineer.dart';
import 'package:wfm_check/data/repository/firestore_repository.dart';
import 'package:wfm_check/data/workorder.dart';
import 'package:wfm_check/utils/shimmer.dart';
import 'package:wfm_check/values/app_colors.dart';

import 'List_shimmer.dart';

class SearchWorkOrderScreen extends StatefulWidget
{
  @override
  _SearchWorkOrderScreenState createState() => _SearchWorkOrderScreenState();
}

class _SearchWorkOrderScreenState extends State<SearchWorkOrderScreen> {

  final databaseReference  = FirebaseFirestore.instance;
  final keywordController = TextEditingController();
  Engineer engineer;
  List<Engineer> items = List<Engineer>();
  bool isLoading = false;
  String keyword = "";
  String fieldName = "Order Number";
  String fieldValue = "ordernumber";

  FirestoreRepository repository;



  @override
  void dispose() {
    keywordController.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {

    repository = FirestoreRepository();


    engineer = ModalRoute.of(context).settings.arguments;

    return Scaffold(
        appBar: AppBar(
          title: Text(engineer.firstName, style: TextStyle(color: AppColors.darkTextColor, fontWeight: FontWeight.bold),),
          backgroundColor: Colors.transparent,
          iconTheme: IconThemeData(
            color: AppColors.darkTextColor, //change your color here
          ),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: IconButton(
                  onPressed: () {

                    buildBottomSheetDialog(context);
                  },
                  icon: SvgPicture.asset("assets/images/id_card.svg", width: 20, height: 20, color: AppColors.blackAlpha90,),
                )
            ),
          ],
          titleSpacing: 0,
          elevation: 0,
        ),
        body: BlocProvider(
          create: (context) => HomeBloc(repository: FirestoreRepository()),
          child:
          BlocListener<HomeBloc, HomeState>(
              listener: (context, state) {
                if(state is SearchEngineerSuccessState){
                  setState(() {
                    items = state.data;
                  });
                }
              },
              child: BlocBuilder<HomeBloc, HomeState>(
                builder: (context, state) {
                  return buildContent(context, state);
                },
              )),
        )
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget buildNoResult() => Container(
    width: double.infinity,
    margin: EdgeInsets.only(top: 64),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SvgPicture.asset("assets/images/no_data.svg", width: 150, height: 150,),
        Padding(
          padding: EdgeInsets.only(top: 62, bottom: 8),
          child: Text("Oops .. ", style: TextStyle(color: AppColors.textInactive.withAlpha(120),  fontSize: 20, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
        ),
        Text("Work order tidak ditemukan", style: TextStyle(color: AppColors.textInactive.withAlpha(120),  fontSize: 16), textAlign: TextAlign.center,),
      ],
    ),
  );

  Widget buildContent(BuildContext context, HomeState state) {
    return SingleChildScrollView(
      child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              buildSearchWidget(context),

              StreamBuilder(
                stream:
                keyword == "" ? databaseReference
                    .collection('User')
                    .doc(engineer.individualId)
                    .collection("WorkOrder")
                    .snapshots() :
                databaseReference
                    .collection('User')
                    .doc(engineer.individualId)
                    .collection("WorkOrder")
                    .where(fieldValue, isEqualTo: keyword)
                    .snapshots(),
                builder: (context, snapshot) {
                  {
                    return  !snapshot.hasData ? ListView.builder(
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        itemCount: 10,
                        itemBuilder: (context, index) => Shimmer.fromColors(
                            baseColor: AppColors.greyDefault,
                            highlightColor: AppColors.whiteSoft,
                            child:

                            ListShimmer(index: -1)
                        )
                    )
                        : snapshot.data.docs.length > 0 ? ListView.builder(

                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        itemBuilder: (context, index) => buildItemList(
                            WorkOrder.fromDocument(snapshot.data.docs[index])
                        ),
                        itemCount: snapshot.data.docs.length
                    )
                        :  buildNoResult();
                  }
                },
              ) ,

            ],
          )
      ),
    );
  }

  Widget buildSearchWidget( BuildContext context) {
    final widthWidget = MediaQuery
        .of(context)
        .size
        .width - 64;
    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: AppColors.greyDefault,


        borderRadius: BorderRadius.circular(18),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          Padding(
            padding: EdgeInsets.only(top: 4, bottom: 8),
            child: Text("Search Work Order", style: TextStyle(
                color: AppColors.darkTextColor,
                fontWeight: FontWeight.bold,
                fontSize: 18),),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 16),
            child: Divider(
              height: 1, color: AppColors.greyLight80.withAlpha(100),),
          ),
          Row(
            children: [

              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 4, bottom: 4),
                    child: Text("Keyword", style: TextStyle(
                        color: AppColors.greyText,
                        fontWeight: FontWeight.bold,
                        fontSize: 12),),
                  ),
                  Container(
                    width: (widthWidget - 16) * 0.6,
                    height: 45,

                    padding: const EdgeInsets.only(left: 12, right: 8),
                    decoration: BoxDecoration(
                      color: AppColors.greyLight80,


                      borderRadius: BorderRadius.circular(28),
                    ),
                    child: TextFormField(
                      autocorrect: false,
                      autofocus: false,
                      maxLines: 1,
                      textInputAction: TextInputAction.search,
                      controller: keywordController,


                      decoration: InputDecoration(
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        hintStyle: TextStyle(color: AppColors.greyText),
                        hintText: "Input keyword . .",

                      ),
                    ),
                  ),
                ],
              ),
              Spacer(),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 4, bottom: 4),
                    child: Text("Search by", style: TextStyle(
                        color: AppColors.greyText,
                        fontWeight: FontWeight.bold,
                        fontSize: 12),),
                  ),
                  Container(
                    width: (widthWidget - 16) * 0.4,
                    height: 45,
                    padding: const EdgeInsets.only(left: 12, right: 8),
                    decoration: BoxDecoration(
                      color: AppColors.greyLight80,


                      borderRadius: BorderRadius.circular(28),
                    ),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: fieldName,
                        style: TextStyle(color: Colors.black, fontSize: 12),

                        items: <String>[
                          'Order Number',
                          'Cust Name',
                          'SIA',
                          'Order id',

                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        hint: Text(
                          "Search by",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w600),
                        ),
                        onChanged: (String value) {
                          setState(() {
                            fieldName = value;
                            value == "Order Number" ? fieldValue = "ordernumber"
                                : value == "Cust Name" ?
                            fieldValue = "nama_cust"
                                : value == "SIA" ?
                            fieldValue = "customerServiceInstanceID"
                                : fieldValue = "id";
                          });
                        },
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),


          Padding(
            padding: EdgeInsets.only(top: 16),
            child: SizedBox(
              width: double.infinity,
              child: MaterialButton(
                height: 45,
                padding: EdgeInsets.only(left: 8, right: 8),
                elevation: 0,
                child: Text('Search',),
                textColor: Colors.white,
                color: Colors.blue,
                shape: RoundedRectangleBorder(

                  borderRadius: BorderRadius.circular(28),

                ),
                onPressed: () {
                  setState(() {
                    keyword = fieldValue=="ordernumber" ? keywordController.text.toUpperCase():fieldValue=="nama_cust" ?  " "+keywordController.text+" ": keywordController.text;
                  });
                  print("field "+fieldValue+" keyword "+keyword);
                  // BlocProvider.of<HomeBloc>(context).add(SearchEngineerEvent(keyword: keyword));
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildItemList(WorkOrder item) =>  Container(
      clipBehavior: Clip.hardEdge,
      margin: EdgeInsets.fromLTRB(0, 16, 0, 0),
      decoration: BoxDecoration(
        color: AppColors.greyLight80.withAlpha(80),
        borderRadius: BorderRadius.all(Radius.circular(20)),

      ),
      child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              Map map = Map();
              map['individualId'] = engineer.individualId;
              map['workOrder'] = item;
              Navigator.of(context).pushNamed("/taskWorkOrderScreen", arguments: map);
            },
            child: Padding(
              padding: EdgeInsets.fromLTRB(8, 8, 16, 8),
              child: Row(
                children: [
                  Container(
                    width: 80,
                    height: 80,
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(right: 16),
                    decoration: BoxDecoration(
                      color: AppColors.yellowLight,
                      borderRadius: BorderRadius.all(Radius.circular(18)),

                    ),
                    child: SvgPicture.asset("assets/images/file.svg", color: AppColors.yellow, width: 24, height: 24,),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(item.ordernumber, style: TextStyle(fontWeight: FontWeight.bold, color: AppColors.darkTextColor, fontSize: 14),),
                        Padding(padding: EdgeInsets.only(right: 8),child: Text(item.namaCust.trim(), maxLines: 1, style: TextStyle(color: AppColors.darkTextColor, fontSize: 14), overflow: TextOverflow.ellipsis)),

                        Padding(padding: EdgeInsets.only(top: 8),child: Text(item.dateTime, style: TextStyle(color: AppColors.grey, fontSize: 14),),)




                      ],
                    ),
                  )
                ],
              ),
            ),
          )
      )
  );


  void buildBottomSheetDialog(context){
    showModalBottomSheet(

        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topRight: Radius.circular(20.0), topLeft: Radius.circular(20.0)),
        ),
        backgroundColor: Colors.white,
        context: context,
        builder: (BuildContext bc){
          return Container(
            child: new Column(
              children: <Widget>[

                Row(
                  children: <Widget>[
                    IconButton(
                      padding: EdgeInsets.only(left: 16),
                      icon: Icon(Icons.close, color: AppColors.blackAlpha90,),
                      onPressed: ((){
                        Navigator.pop(context);
                      }),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 120,
                      margin: EdgeInsets.only(top: 20, bottom: 20),
                      alignment: Alignment.center,
                      child: Text("User Detail", style: TextStyle(fontWeight: FontWeight.normal),),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16, right: 16),
                  child: Divider(color: AppColors.greyLight, height: 1,),
                ),

                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        new ListTile(
                            title: new Text('Team Name', style: TextStyle( color: AppColors.textInactive, fontSize: 12,),),
                            // tileColor: AppColors.textInactive,
                            subtitle: new Text(engineer.firstName ?? "", style: TextStyle( color: AppColors.darkTextColor,),),
                            onTap: () => {
                              copyToClipboard(engineer.firstName ?? "", context)
                            }
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Divider(color: AppColors.greyLight, height: 1,),
                        ),
                        new ListTile(
                            title: new Text('Username', style: TextStyle( color: AppColors.textInactive, fontSize: 12,),),
                            subtitle: new Text(engineer.username ?? "", style: TextStyle( color: AppColors.darkTextColor,),),
                            onTap: () => {
                              copyToClipboard(engineer.username ?? "", context)
                            }
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Divider(color: AppColors.greyLight, height: 1,),
                        ),
                        new ListTile(
                            title: new Text('Password', style: TextStyle( color: AppColors.textInactive, fontSize: 12,),),
                            // tileColor: AppColors.textInactive,
                            subtitle: new Text(engineer.password ?? "", style: TextStyle( color: AppColors.darkTextColor,),),
                            onTap: () => {
                              copyToClipboard(engineer.password ?? "", context)
                            }
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Divider(color: AppColors.greyLight, height: 1,),
                        ),
                        new ListTile(
                            title: new Text('Profit Center', style: TextStyle( color: AppColors.textInactive, fontSize: 12,),),
                            // tileColor: AppColors.textInactive,
                            subtitle: new Text(engineer.profitCenter ?? "", style: TextStyle( color: AppColors.darkTextColor,),),
                            onTap: () => {
                              copyToClipboard(engineer.profitCenter ?? "", context)
                            }
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Divider(color: AppColors.greyLight, height: 1,),
                        ),
                        new ListTile(
                            title: new Text('Token', style: TextStyle( color: AppColors.textInactive, fontSize: 12,),),
                            // tileColor: AppColors.textInactive,
                            subtitle: new Text(engineer.token ?? "", style: TextStyle( color: AppColors.darkTextColor,),),
                            onTap: () => {
                              copyToClipboard(engineer.token ?? "", context)
                            }
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Divider(color: AppColors.greyLight, height: 1,),
                        ),
                        new ListTile(
                            title: new Text('ID', style: TextStyle( color: AppColors.textInactive, fontSize: 12,),),
                            // tileColor: AppColors.textInactive,
                            subtitle: new Text(engineer.individualId, style: TextStyle( color: AppColors.darkTextColor,),),
                            onTap: () => {
                              copyToClipboard(engineer.individualId ?? "", context)
                            }
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16, right: 16, bottom: 32),
                          child: Divider(color: AppColors.greyLight, height: 1,),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        }
    );

  }

  void copyToClipboard(String text, BuildContext context){

    try{
      FlutterClipboard.copy(
          text)
          .then((result) {
        Toast.show('Copied to Clipboard', context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);

      });
    }catch(e){
      print(e);
    }

  }
}