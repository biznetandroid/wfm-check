
import 'package:flutter/widgets.dart';
import 'package:wfm_check/values/app_colors.dart';

class ListShimmer extends StatelessWidget {
  final int index;
  const ListShimmer({Key key, this.index});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          height: 20,
          margin: EdgeInsets.fromLTRB(0, 16, 0, 8),
          width: MediaQuery.of(context).size.width - 100,
          decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10.0)),
            color: AppColors.colorForeground,
          ),
        ),
        Container(
          height: 15,
          margin: EdgeInsets.fromLTRB(0, 0, 0, 16),
          width: MediaQuery.of(context).size.width - 180,
          decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(6.0)),
            color: AppColors.colorForeground,
          ),
        )
      ],
    );
  }
}