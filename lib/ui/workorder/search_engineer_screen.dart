
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:wfm_check/bloc/home/home_bloc.dart';
import 'package:wfm_check/data/engineer.dart';
import 'package:wfm_check/data/repository/firestore_repository.dart';
import 'package:wfm_check/utils/shimmer.dart';
import 'package:wfm_check/values/app_colors.dart';

import 'List_shimmer.dart';

class SearchEngineerScreen extends StatefulWidget
{

  @override
  _SearchEngineerScreenState createState() => _SearchEngineerScreenState();
}

class _SearchEngineerScreenState extends State<SearchEngineerScreen> {
  final databaseReference  = FirebaseFirestore.instance;
  final keywordController = TextEditingController();
  List<Engineer> items = List<Engineer>();
  bool isLoading = false;
  String keyword = "";
  String fieldName = "Username";
  String fieldValue = "username";

  FirestoreRepository repository;



  @override
  void dispose() {
    keywordController.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    repository = FirestoreRepository();
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        title: Text("Select User", style: TextStyle(color: AppColors.darkTextColor, fontWeight: FontWeight.bold),),
        backgroundColor: Colors.transparent,
        titleSpacing: 0,
        elevation: 0,
        iconTheme: IconThemeData(
          color: AppColors.darkTextColor, //change your color here
        ),
      ),
      body: BlocProvider(
        create: (context) => HomeBloc(repository: FirestoreRepository()),
        child:
        BlocListener<HomeBloc, HomeState>(
            listener: (context, state) {
              if(state is SearchEngineerSuccessState){
                setState(() {
                  items = state.data;
                });
              }
            },
            child: BlocBuilder<HomeBloc, HomeState>(
              builder: (context, state) {
                return buildContent(context, state);
              },
            )),
      )
       // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget buildNoResult() => Container(
    width: double.infinity,
    padding: EdgeInsets.only(top:64),
    child: SingleChildScrollView(child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SvgPicture.asset("assets/images/no_data.svg", width: 150, height: 150,),
        Padding(
          padding: EdgeInsets.only(top: 62, bottom: 8),
          child: Text("Oops .. ", style: TextStyle(color: AppColors.textInactive.withAlpha(120),  fontSize: 20, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
        ),
        Text("User tidak ditemukan", style: TextStyle(color: AppColors.textInactive.withAlpha(120),  fontSize: 16), textAlign: TextAlign.center,),
      ],
    ),),
  );

  Widget buildContent(BuildContext context, HomeState state) {
    final widthWidget = MediaQuery.of(context).size.width - 64;
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: AppColors.greyDefault,


              borderRadius: BorderRadius.circular(18),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(top:4, bottom: 8),
                  child: Text("Search User", style: TextStyle(color: AppColors.darkTextColor, fontWeight: FontWeight.bold, fontSize: 18 ), ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 16),
                  child: Divider(height: 1, color:AppColors.greyLight80.withAlpha(100), ),
                ),

                Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left:4, bottom: 4),
                          child: Text("Keyword", style: TextStyle(color: AppColors.greyText, fontWeight: FontWeight.bold, fontSize: 12 ), ),
                        ),
                        Container(
                          width: (widthWidget-16)*0.6,
                          height: 45,

                          padding: const EdgeInsets.only(left: 12, right: 8),
                          decoration: BoxDecoration(
                            color: AppColors.greyLight80,


                            borderRadius: BorderRadius.circular(28),
                          ),
                          child: TextFormField(
                            autocorrect: false,
                            autofocus: false,
                            maxLines: 1,
                            textInputAction: TextInputAction.search,
                            controller: keywordController,


                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              hintStyle: TextStyle(color: AppColors.greyText),
                              hintText: "Input keyword . .",

                            ),
                          ),
                        ),
                      ],
                    ),
                    Spacer(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left:4, bottom: 4),
                          child: Text("Search by", style: TextStyle(color: AppColors.greyText, fontWeight: FontWeight.bold, fontSize: 12 ), ),
                        ),
                        Container(
                          width: (widthWidget-16)*0.4,
                          height: 45,
                          padding: const EdgeInsets.only(left: 12, right: 8),
                          decoration: BoxDecoration(
                            color: AppColors.greyLight80,


                            borderRadius: BorderRadius.circular(28),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              value: fieldName,
                              style: TextStyle(color: Colors.black),

                              items: <String>[
                                'Username',
                                'Team name',
                                'ID',

                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              hint: Text(
                                "Search by",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600),
                              ),
                              onChanged: (String value) {
                                setState(() {
                                  fieldName = value;
                                  value == "Username" ? fieldValue = "username"
                                      : value == "ID" ? fieldValue = "individualId"
                                      : fieldValue = "firstName";
                                });
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),




                Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: SizedBox(
                    width: double.infinity,
                    child: MaterialButton(
                      height: 45,
                      padding: EdgeInsets.only(left: 8, right: 8),
                      elevation: 0,
                      child: Text('Search',),
                      textColor: Colors.white,
                      color: Colors.blue,
                      shape: RoundedRectangleBorder(

                        borderRadius: BorderRadius.circular(28),

                      ),
                      onPressed: () {
                        setState(() {
                          keyword = keywordController.text;

                        });
                        // BlocProvider.of<HomeBloc>(context).add(SearchEngineerEvent(keyword: keyword));
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),

          Expanded(
              child:
              keyword != "" ? StreamBuilder(
                stream:
                // fieldValue == "id" ?databaseReference
                //     .collection('User')
                //     .doc(keyword).parent
                //     .snapshots() :
                databaseReference
                    .collection('User')
                    .where(fieldValue, isEqualTo: keyword)
                    .snapshots(),
                builder: (context, snapshot) {
                  {
                    return  !snapshot.hasData ? ListView.builder(
                        itemCount: 10,
                        itemBuilder: (context, index) => Shimmer.fromColors(
                            baseColor: AppColors.greyDefault,
                            highlightColor: AppColors.whiteSoft,
                            child:

                            ListShimmer(index: -1)
                        )
                    )
                        : snapshot.data.docs.length <=0 ?buildNoResult()
                        :ListView.builder(
                          itemBuilder: (context, index) => buildItemList(
                            Engineer.fromDocument(snapshot.data.docs[index])
                          ),
                          itemCount: snapshot.data.docs.length
                        );
                  }
                },
              ): Container() ,

          )
        ],
      )
    );
  }

  Widget buildItemList(Engineer item) =>  Container(
      clipBehavior: Clip.hardEdge,
      margin: EdgeInsets.fromLTRB(0, 16, 0, 0),
      decoration: BoxDecoration(
        color: AppColors.greyLight80,
        borderRadius: BorderRadius.all(Radius.circular(20)),

      ),
        child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {
                Navigator.of(context).pushNamed("/searchWorkOrderScreen", arguments: item);
              },
              child: Padding(
                padding: EdgeInsets.fromLTRB(8, 8, 16, 8),
                child: Row(
                  children: [
                    Container(
                      width: 80,
                      height: 80,
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(right: 16),
                      decoration: BoxDecoration(
                        color: AppColors.white.withAlpha(100),
                        borderRadius: BorderRadius.all(Radius.circular(18)),
    
                      ),
                      child: SvgPicture.asset("assets/images/user.svg", color: AppColors.darkTextColor, width: 24, height: 24,),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(item.firstName, style: TextStyle(fontWeight: FontWeight.bold, color: AppColors.darkTextColor, fontSize: 14),),
                        Text(item.username, style: TextStyle(color: AppColors.darkTextColor, fontSize: 14),),
                        
                        Padding(padding: EdgeInsets.only(top: 8),child: Text(item.individualId, style: TextStyle(color: AppColors.grey, fontSize: 14),),)




                      ],
                    )
                  ],
                ),
              ),
            )
        )
    );
}