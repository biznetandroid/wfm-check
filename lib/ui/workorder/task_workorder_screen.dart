

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:wfm_check/bloc/home/home_bloc.dart';
import 'package:wfm_check/data/repository/firestore_repository.dart';
import 'package:wfm_check/data/task.dart';
import 'package:wfm_check/data/workorder.dart';
import 'package:wfm_check/values/app_colors.dart';
import 'package:wfm_check/values/const/task_const.dart';
import 'package:wfm_check/values/const/work_order_type_const.dart';
import 'package:toast/toast.dart';
import 'package:clipboard/clipboard.dart';

class TaskWorkOrderScreen extends StatefulWidget
{
  @override
  _TaskWorkOrderScreenState createState() => _TaskWorkOrderScreenState();
}

class _TaskWorkOrderScreenState extends State<TaskWorkOrderScreen> {

  final databaseReference  = FirebaseFirestore.instance;
  WorkOrder workOrder;
  List<Task> items = List<Task>();
  bool isLoading = false;
  String individualId;



  @override
  void initState() {

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {


    Map data = ModalRoute.of(context).settings.arguments;
    workOrder = data["workOrder"];
    individualId = data["individualId"];


    return Scaffold(
        appBar: AppBar(
          title: Text(workOrder.namaCust, style: TextStyle(color: AppColors.darkTextColor, fontWeight: FontWeight.bold),),
          backgroundColor: Colors.transparent,
          iconTheme: IconThemeData(
            color: AppColors.darkTextColor, //change your color here
          ),
          titleSpacing: 0,
          elevation: 0,
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: IconButton(
                  onPressed: () {

                    buildBottomSheetDialog(context);
                  },
                  icon: SvgPicture.asset("assets/images/ic_info.svg", width: 20, height: 20, color: AppColors.blackAlpha90,),
                )
            ),
          ],
        ),
        body: BlocProvider(
          create: (context) => HomeBloc(repository: FirestoreRepository())..add(InitialHomeEvent(userId: individualId, workId: workOrder.id)),
          child:
          BlocListener<HomeBloc, HomeState>(
              listener: (context, state) {
                if(state is GetTaskSuccessState){
                  setTaskList(state.data);
                }else if(state is CheklistCompleteState){
                  if(state.success){
                    BlocProvider.of<HomeBloc>(context).add(
                        InitialHomeEvent(userId: individualId, workId: workOrder.id)
                    );
                  }
                }
              },
              child: BlocBuilder<HomeBloc, HomeState>(
                builder: (context, state) {
                  return buildContent(context);
                },
              )),
        )
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget buildNoResult() => Container(
    width: double.infinity,
    height: double.infinity,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SvgPicture.asset("assets/images/no_data.svg", width: 150, height: 150,),
        Padding(
          padding: EdgeInsets.only(top: 62, bottom: 8),
          child: Text("Oops .. ", style: TextStyle(color: AppColors.textInactive.withAlpha(120),  fontSize: 20, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
        ),
        Text("Work order tidak ditemukan", style: TextStyle(color: AppColors.textInactive.withAlpha(120),  fontSize: 16), textAlign: TextAlign.center,),
      ],
    ),
  );

  Widget buildContent(BuildContext context) {
    final widthWidget = MediaQuery.of(context).size.width - 32;
    
    return SingleChildScrollView(
      child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  Container(
                    height: 120,
                    margin: EdgeInsets.only(left: 16),
                    width: (widthWidget-16)/2,
                    child:
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Order number", style: TextStyle(color: AppColors.greyText, fontSize: 10),),
                        Flexible(child: Text(workOrder.ordernumber, style: TextStyle(color: AppColors.darkTextColor, fontSize: 14),)),

                        Padding(
                          padding:EdgeInsets.only(top: 8),
                          child: Text("Type", style: TextStyle(color: AppColors.greyText, fontSize: 10),),
                        ),
                        Flexible(child: Text(workOrder.type, style: TextStyle(color: AppColors.darkTextColor, fontSize: 14),)),
                      ],
                    ),
                  ),

                  Container(
                    height: 120,
                    width: (widthWidget-16)/2,
                    child:
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Created Date", style: TextStyle(color: AppColors.greyText, fontSize: 10),),
                        Flexible(child: Text(workOrder.dateTime, style: TextStyle(color: AppColors.darkTextColor, fontSize: 14),)),


                        Padding(
                          padding:EdgeInsets.only(top: 8),
                          child: Text("Status", style: TextStyle(color: AppColors.greyText, fontSize: 10),),
                        ),
                    Container(
                      height: 24,
                      decoration: BoxDecoration(
                        color: workOrder.status=="Perform Field Activity"? AppColors.green.withAlpha(60): AppColors.yellowLight,
                        borderRadius: BorderRadius.circular(18),
                      ),
                      margin: EdgeInsets.only(top: 2),
                      padding:EdgeInsets.only(left: 16, right:16, top:4, bottom: 4),
                      child:Text(workOrder.status, style: TextStyle(color: AppColors.darkTextColor, fontSize: 10),),
                    ),
                      ],
                    ),
                  )
                ],

              ),
              GridView.builder(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                padding: EdgeInsets.only(left: 16, right: 16, top: 32),
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 200,
                    childAspectRatio: 3 / 2,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20),
                itemCount: items.length,
                itemBuilder: (context, index) {
                  final item = items[index];
                  return Container(
                    clipBehavior: Clip.hardEdge,
                    decoration: BoxDecoration(
                      color: AppColors.greyLight80,
                      borderRadius: BorderRadius.circular(18),
                    ),
                    alignment: Alignment.center,
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                          onTap: () {
                            if(item.done){
                              item.done = false;
                              BlocProvider.of<HomeBloc>(context).add(
                                  UnchecklistEvent(userId: individualId, task: item)
                              );
                            }else{
                              item.done = true;
                              BlocProvider.of<HomeBloc>(context).add(
                                  ChecklistEvent(userId: individualId, task: item)
                              );
                            }
                          },
                          child: SizedBox(
                            height: double.infinity,
                            width: double.infinity,
                            child: Stack(
                              children: [
                                Positioned(
                                  left: 16,
                                  right: 16,
                                  top: 8,
                                  child: Text(item.alias, style: TextStyle(fontWeight: FontWeight.bold, color: AppColors.darkTextColor, fontSize: 14),),
                                ),

                                item.done? Positioned(
                                  bottom: 8,
                                  right: 8,
                                  child: ClipOval(
                                      child: Container(
                                        width: 30,
                                        height: 30,
                                        color: AppColors.green,
                                        child: SvgPicture.asset("assets/images/checked.svg", color: AppColors.white, width: 30, height: 30,),
                                      )
                                  )
                                  ,
                                ) : Text( ""),

                              ],
                            ),
                          )
                      ),
                    ),
                  );
                },

              )
            ],
          )
      ),
    );
  }

  void setTaskList(List<Task> tasks){
    items.clear();

    int indexTask= tasks.indexWhere((item) => item.done && item.name == CHECKLIST_TASK);
    items.add(
        Task(
            alias: "Checklist Activity",
            name: CHECKLIST_TASK,
            done: indexTask>=0 ? tasks[indexTask].done : false,
            workId: workOrder.id
        )
    );


    indexTask = tasks.indexWhere((item) => item.done && item.name == EQUIPMENT_MANAGEMENT_TASK);
    items.add(
        Task(
            alias: "Equipment Management",
            name: EQUIPMENT_MANAGEMENT_TASK,
            done: indexTask>=0 ? tasks[indexTask].done : false,
            workId: workOrder.id
        )
    );

    indexTask = tasks.indexWhere((item) => item.done && item.name == REGISTER_MODEM_TASK);
    items.add(
        Task(
            alias: "Register Modem",
            name: REGISTER_MODEM_TASK,
            done: indexTask>=0 ? tasks[indexTask].done : false,
            workId: workOrder.id
        )
    );

    if(workOrder.type == RELOCATION){
      indexTask = tasks.indexWhere((item) => item.done && item.name == RELOCATE_SERVICE_TASK);
      items.add(
          Task(
              alias: "Relocate Service",
              name: RELOCATE_SERVICE_TASK,
              done: indexTask>=0 ? tasks[indexTask].done : false,
              workId: workOrder.id
          )
      );
    }else if(workOrder.type == CHANGE_PLAN){
      indexTask = tasks.indexWhere((item) => item.done && item.name == CHANGE_PLAN_TASK);
      items.add(
          Task(
              alias: "Change Plan",
              name: CHANGE_PLAN_TASK,
              done: indexTask>=0 ? tasks[indexTask].done : false,
              workId: workOrder.id
          )
      );
    }else if(workOrder.type == INSTALLATION || workOrder.type == RELOCATION || workOrder.type == TROUBLESHOOT){
      indexTask = tasks.indexWhere((item) => item.done && item.name == ACTIVATION_SERVICE_TASK);
      items.add(
          Task(
              alias: "Activation Service",
              name: ACTIVATION_SERVICE_TASK,
              done: indexTask>=0 ? tasks[indexTask].done : false,
              workId: workOrder.id
          )
      );
    }

    indexTask = tasks.indexWhere((item) => item.done && item.name == CUSTOMER_SIGN_TASK);
    items.add(
        Task(
            alias: "Customer's Signature",
            name: CUSTOMER_SIGN_TASK,
            done: indexTask>=0 ? tasks[indexTask].done : false,
            workId: workOrder.id
        )
    );

    indexTask = tasks.indexWhere((item) => item.done && item.name == REPORT_TASK);
    items.add(
        Task(
            alias: "Report",
            name: REPORT_TASK,
            done: indexTask>=0 ? tasks[indexTask].done : false,
            workId: workOrder.id
        )
    );

    indexTask = tasks.indexWhere((item) => item.done && item.name == UPLOAD_IMAGE_TASK);
    items.add(
        Task(
            alias: "Upload Image",
            name: UPLOAD_IMAGE_TASK,
            done: indexTask>=0 ? tasks[indexTask].done : false,
            workId: workOrder.id
        )
    );

    if(workOrder.type != TERMINATION){
      indexTask = tasks.indexWhere((item) => item.done && item.name == SELECT_PORT_TASK);
      items.add(
          Task(
              alias: "Select Port",
              name: SELECT_PORT_TASK,
              done: indexTask>=0 ? tasks[indexTask].done : false,
              workId: workOrder.id
          )
      );

      indexTask = tasks.indexWhere((item) => item.done && item.name == SUBMIT_DP_PORT_TASK);
      items.add(
          Task(
              alias: "Submit DP Port",
              name: SUBMIT_DP_PORT_TASK,
              done: indexTask>=0 ? tasks[indexTask].done : false,
              workId: workOrder.id
          )
      );

      indexTask = tasks.indexWhere((item) => item.done && item.name == CABLE_SPEED_TASK);
      items.add(
          Task(
              alias: "Cable & Speed Test",
              name: CABLE_SPEED_TASK,
              done: indexTask>=0 ? tasks[indexTask].done : false,
              workId: workOrder.id
          )
      );

      indexTask = tasks.indexWhere((item) => item.done && item.name == PRINT_LABEL_TASK);
      items.add(
          Task(
              alias: "Print Label",
              name: PRINT_LABEL_TASK,
              done: indexTask>=0 ? tasks[indexTask].done : false,
              workId: workOrder.id
          )
      );


    }

    if(workOrder.type == TROUBLESHOOT){
      indexTask = tasks.indexWhere((item) => item.done && item.name == INTERNAL_PROBLEM_TASK);
      items.add(
          Task(
              alias: "Internal Problem",
              name: INTERNAL_PROBLEM_TASK,
              done: indexTask>=0 ? tasks[indexTask].done : false,
              workId: workOrder.id
          )
      );

      indexTask = tasks.indexWhere((item) => item.done && item.name == PRINT_EQUIPMENT_REPLACEMENT_TASK);
      items.add(
          Task(
              alias: "Print Equipment Replacement",
              name: PRINT_EQUIPMENT_REPLACEMENT_TASK,
              done: indexTask>=0 ? tasks[indexTask].done : false,
              workId: workOrder.id
          )
      );
    }


    if(workOrder.type == TERMINATION){

      indexTask = tasks.indexWhere((item) => item.done && item.name == REMOVE_DP_PORT_TASK);
      items.add(
          Task(
              alias: "Remove DP Port",
              name: REMOVE_DP_PORT_TASK,
              done: indexTask>=0 ? tasks[indexTask].done : false,
              workId: workOrder.id
          )
      );

      indexTask = tasks.indexWhere((item) => item.done && item.name == DISMANTLE_TASK);
      items.add(
          Task(
              alias: "Dismantle",
              name: DISMANTLE_TASK,
              done: indexTask>=0 ? tasks[indexTask].done : false,
              workId: workOrder.id
          )
      );
    }

  }

  void buildBottomSheetDialog(context){
    showModalBottomSheet(

        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topRight: Radius.circular(20.0), topLeft: Radius.circular(20.0)),
        ),
        backgroundColor: Colors.white,
        context: context,
        builder: (BuildContext bc){
          return Container(
            child: new Column(
              children: <Widget>[

                Row(
                  children: <Widget>[
                    IconButton(
                      padding: EdgeInsets.only(left: 16),
                      icon: Icon(Icons.close, color: AppColors.blackAlpha90,),
                      onPressed: ((){
                        Navigator.pop(context);
                      }),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 120,
                      margin: EdgeInsets.only(top: 20, bottom: 20),
                      alignment: Alignment.center,
                      child: Text("Work Order Detail", style: TextStyle(fontWeight: FontWeight.normal),),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16, right: 16),
                  child: Divider(color: AppColors.greyLight, height: 1,),
                ),

                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        new ListTile(
                            title: new Text('Work Order ID', style: TextStyle( color: AppColors.textInactive, fontSize: 12,),),
                            // tileColor: AppColors.textInactive,
                            subtitle: new Text(workOrder.id ?? "", style: TextStyle( color: AppColors.darkTextColor,),),
                            onTap: () => {
                              copyToClipboard(workOrder.id ?? "", context)
                            }
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Divider(color: AppColors.greyLight, height: 1,),
                        ),
                        new ListTile(
                            title: new Text('Order Number', style: TextStyle( color: AppColors.textInactive, fontSize: 12,),),
                            subtitle: new Text(workOrder.ordernumber ?? "", style: TextStyle( color: AppColors.darkTextColor,),),
                            onTap: () => {
                              copyToClipboard(workOrder.ordernumber ?? "", context)
                            }
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Divider(color: AppColors.greyLight, height: 1,),
                        ),
                        new ListTile(
                            title: new Text('Work Key', style: TextStyle( color: AppColors.textInactive, fontSize: 12,),),
                            // tileColor: AppColors.textInactive,
                            subtitle: new Text(workOrder.workKey ?? "", style: TextStyle( color: AppColors.darkTextColor,),),
                            onTap: () => {
                              copyToClipboard(workOrder.workKey ?? "", context)
                            }
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Divider(color: AppColors.greyLight, height: 1,),
                        ),
                        new ListTile(
                            title: new Text('Service Instance Account', style: TextStyle( color: AppColors.textInactive, fontSize: 12,),),
                            // tileColor: AppColors.textInactive,
                            subtitle: new Text(workOrder.customerServiceInstanceId ?? "", style: TextStyle( color: AppColors.darkTextColor,),),
                            onTap: () => {
                              copyToClipboard(workOrder.customerServiceInstanceId ?? "", context)
                            }
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Divider(color: AppColors.greyLight, height: 1,),
                        ),
                        new ListTile(
                            title: new Text('Customer Account Number', style: TextStyle( color: AppColors.textInactive, fontSize: 12,),),
                            // tileColor: AppColors.textInactive,
                            subtitle: new Text(workOrder.custid ?? "", style: TextStyle( color: AppColors.darkTextColor,),),
                            onTap: () => {
                              copyToClipboard(workOrder.custid ?? "", context)
                            }
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Divider(color: AppColors.greyLight, height: 1,),
                        ),
                        new ListTile(
                            title: new Text('Customer Name', style: TextStyle( color: AppColors.textInactive, fontSize: 12,),),
                            // tileColor: AppColors.textInactive,
                            subtitle: new Text(workOrder.namaCust, style: TextStyle( color: AppColors.darkTextColor,),),
                            onTap: () => {
                              copyToClipboard(workOrder.namaCust ?? "", context)
                            }
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Divider(color: AppColors.greyLight, height: 1,),
                        ),
                        new ListTile(
                            title: new Text('Product Name', style: TextStyle( color: AppColors.textInactive, fontSize: 12,),),
                            // tileColor: AppColors.textInactive,
                            subtitle: new Text(workOrder.productname, style: TextStyle( color: AppColors.darkTextColor,),),
                            onTap: () => {
                              copyToClipboard(workOrder.productname ?? "", context)
                            }
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Divider(color: AppColors.greyLight, height: 1,),
                        ),
                        new ListTile(
                            title: new Text('Start Time', style: TextStyle( color: AppColors.textInactive, fontSize: 12,),),
                            // tileColor: AppColors.textInactive,
                            subtitle: new Text(workOrder.startTime, style: TextStyle( color: AppColors.darkTextColor,),),
                            onTap: () => {
                              copyToClipboard(workOrder.startTime ?? "", context)
                            }
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16, right: 16),
                          child: Divider(color: AppColors.greyLight, height: 1,),
                        ),
                        new ListTile(
                            title: new Text('End Time', style: TextStyle( color: AppColors.textInactive, fontSize: 12,),),
                            // tileColor: AppColors.textInactive,
                            subtitle: new Text(workOrder.endTime, style: TextStyle( color: AppColors.darkTextColor,),),
                            onTap: () => {
                              copyToClipboard(workOrder.endTime ?? "", context)
                            }
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 16, right: 16, bottom: 32),
                          child: Divider(color: AppColors.greyLight, height: 1,),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        }
    );

  }

  void copyToClipboard(String text, BuildContext context){

    try{
      FlutterClipboard.copy(
          text)
          .then((result) {
        Toast.show('Copied to Clipboard', context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);

      });
    }catch(e){
      print(e);
    }

  }


}


