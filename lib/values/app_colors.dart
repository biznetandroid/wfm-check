import 'package:flutter/material.dart';

class AppColors {
  static const Color colorBackground = Color(0xff1A202E);
  static const Color colorBackgroundDark = Color(0xff141924);
  static const Color colorForeground = Color(0xff27303F);
  static const Color colorForegroundLight = Color(0xff3c4758);
  static const Color red = Color(0xffE26262);
  static const Color redSoft = Color(0xffFDE8E8);
  static const Color colorAccent = Color(0xff0694A2);
  static const Color textInactive = Color(0xff64748B);
  static const Color redLoading = Color(0xffF50057);


  static const Color colorPrimaryDark = Color(0xff25378d);
  static const Color colorPrimary = Color(0xff2263a5);
  static const Color colorAccentSecondary = Color(0xffF0B33D);
  static const Color tabIconBlue = Color(0xff25378d);
  static const Color fieldBackground = Color(0xffeef4fc);
  static const Color fieldBackground90 = Color(0x90eef4fc);
  static const Color fieldBtiorder = Color(0xffe5e4f4);

  static const Color blue = Color(0xff25378d);
  static const Color lightBlue = Color(0xff00a9e8);
  static const Color placeholderText = Color(0xffb0b7cc);
  static const Color placeholderText80 = Color(0xff80b0b7cc);

  static const Color white = Color(0xffffffff);
  static const Color whiteSoft = Color(0x70ffffff);
  static const Color grey = Color(0xffb7b7b7);
  static const Color greyLight = Color(0xffbcbcbc);
  static const Color greyLight80 = Color(0x80bcbcbc);
  static const Color grey600 = Color(0x600b7b7b7);
  static const Color greyDefault = Color(0xffe6e6e6);
  static const Color greyText = Color(0xff909090);

  static const Color black = Color(0xff000000);
  static const Color blackAlpha90 = Color(0x90000000);
  static const Color icon_grey = Color(0xffA3A3A3);
  static const Color darkTextColor = Color(0xff414042);

  static const Color orange = Color(0xfff07f00);
  static const Color green = Color(0xff33cc99);
  static const Color yellow = Color(0xfff6a200);
  static const Color yellowLight = Color(0xffe7d8bc);

  static const Color monokaiWhite = Color(0xfffff7fb);
  static const Color monokaiBlack = Color(0xff272822);

  static const Color paletteGreen = Color(0xff2ec9a1);
  static const Color PaletteBlue = Color(0xff00b0ec);

  //profil color
  static const Color profileContainerText = Color(0xffDCDCDC);
  static const Color profileContainerBorder = Color(0xffC4C4C4);
  static const Color profileText = Color(0xff838284);

  static const Color defaultBackground = Color(0xfff6f6f8);
  static const Color bottomNavBackground = Color(0xfffafafa);
}
