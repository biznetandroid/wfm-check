
  const CHECKLIST_TASK = "Checklist Task";
  const EQUIPMENT_MANAGEMENT_TASK = "Equipment Management";
  const REGISTER_MODEM_TASK = "Register Modem";
  const UNREGISTER_MODEM_TASK = "UnRegister Modem";
  const ACTIVATION_SERVICE_TASK = "Activation Modem";
  const CUSTOMER_SIGN_TASK = "Customer's Signature";
  const REPORT_TASK = "Report";
  const UPLOAD_IMAGE_TASK = "Upload Image";
  const CHANGE_PLAN_TASK = "Change Plan";
  const RELOCATE_SERVICE_TASK = "Relocate Service";
  const SELECT_PORT_TASK = "Select Port";
  const CABLE_SPEED_TASK = "Cable Speed";
  const PRINT_LABEL_TASK = "Print Label";
  const DETECT_MODEM_TASK = "Detect Modem";
  const SUBMIT_DP_PORT_TASK = "Submit DP Port";
  const REMOVE_DP_PORT_TASK = "Remove DP Port";
  const INTERNAL_PROBLEM_TASK = "Internal Problem";
  const PRINT_EQUIPMENT_REPLACEMENT_TASK = "Print Equipment Replacement";
  const REMOVE_EQUIPMENT_TASK = "Remove Equipment";
  const DISMANTLE_TASK = "Dismantle";


