

import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:wfm_check/data/engineer.dart';

class FirestoreRepository{

  FirestoreRepository();





  // List<Engineer> getEngineers(String keyword) {
  //   List<Engineer> chatRoom = List<Engineer>();
  //   return FirebaseFirestore.instance
  //       .collection('User')
  //       .where('username', isEqualTo: keyword)
  //       .get()
  //       .withConverter<Engineer>(
  //     fromFirestore: (snapshot, _) => Engineer.fromJson(snapshot.data()),
  //     toFirestore: (model, _) => model.toJson(),
  //       // .then((data)=>{
  //       //   if(data.size>0) chatRoom = data.docs.map((doc) => {
  //       //     Engineer.fromDocument(doc)
  //       //   } ).toList().cast<Engineer>()
  //       //
  //       // }
  //   );
  // }






  Future<void> sendChat({String docId, String userId, String name, String message, List<String> userReceiver}) {
    return FirebaseFirestore.instance
        .collection("chat")
        .doc(docId)
        .collection("messages")
        .doc()
        .set({
      'user_id': userId,
      'name': name,
      'created': Timestamp.now(),
      'message': message
    });
  }

  Future<void> updateRoom(String docId, String userId, String name, String message) {
    return FirebaseFirestore.instance
        .collection("chat")
        .doc(docId)
        .update({
      'last_user_id': userId,
      'last_name': name,
      'created': Timestamp.now(),
      'last_message': message,
    })
        .then((value) => print("Room updated"))
        .catchError((error) => print("Failed to update room: $error"));
  }


}