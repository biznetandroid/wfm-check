

import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:wfm_check/domain/app_module.dart';


class TestApiRepository{

  TestApiRepository();

  Future<Response> sendGet(String url) async {
    try {
      var response = await dio.get(url);
      print(response.statusCode);
      print(response.data);
      return response;
    } catch (e) {
      print("$e");
      return e;
    }
  }

  Future<Response> sendPost(String url, Map data) async {

    try {
      var response = await dio.post(url, data: data);
      print(response.statusCode);
      print(response.data);
      return response;
    } catch (e) {
      print("$e");
      return e;
    }
  }

}