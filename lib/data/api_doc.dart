import 'dart:convert';

ApiDoc apiDocFromJson(String str) => ApiDoc.fromJson(json.decode(str));

String apiDocToJson(ApiDoc data) => json.encode(data.toJson());

class ApiDoc {
  ApiDoc({
    this.name,
    this.url,
    this.method,
    this.app,
    this.request,
  });

  String name;
  String url;
  String method;
  String app;
  List<String> request;

  factory ApiDoc.fromJson(Map<String, dynamic> json) => ApiDoc(
    name: json["name"],
    url: json["url"],
    method: json["method"],
    app: json["app"],
    request: List<String>.from(json["request"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "url": url,
    "method": method,
    "app": app,
    "request": List<dynamic>.from(request.map((x) => x)),
  };
}
