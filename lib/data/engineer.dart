import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

Engineer engineerFromJson(String str) => Engineer.fromJson(json.decode(str));

String engineerToJson(Engineer data) => json.encode(data.toJson());

class Engineer {
  Engineer({
    this.firstName,
    this.username,
    this.password,
    this.individualId,
    this.token,
    this.profitCenter,
  });

  String firstName;
  String username;
  String password;
  String individualId;
  String token;
  String profitCenter;

  factory Engineer.fromDocument(DocumentSnapshot document) {
    return Engineer(
        firstName: document["firstName"],
        username: document["username"],
        password: document["password"],
        individualId: document.id,
        token: document["token"],
        profitCenter:  document.toString().contains("profitCenter")?  document["profitCenter"] : ""
    );
  }

  factory Engineer.fromJson(Map<String, dynamic> json) => Engineer(
    firstName: json["firstName"],
    username:  json["username"],
    password:  json["password"],
    individualId: json["individualId"],
    token: json["token"],
    profitCenter: json["profitCenter"],
  );

  Map<String, dynamic> toJson() => {
    "firstName": firstName,
    "username": username,
    "password": password,
    "individualId": individualId,
    "token": token,
    "profitCenter": profitCenter,
  };
}