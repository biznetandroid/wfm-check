import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

WorkOrder workOrderFromJson(String str) => WorkOrder.fromJson(json.decode(str));

String workOrderToJson(WorkOrder data) => json.encode(data.toJson());

class WorkOrder {
  WorkOrder({
    this.status,
    this.id,
    this.type,
    this.namaCust,
    this.ordernumber,
    this.workKey,
    this.startTime,
    this.endTime,
    this.dateTime,
    this.technology,
    this.productname,
    this.packagetype,
    this.custid,
    this.customerServiceInstanceId,
  });

  String status;
  String id;
  String type;
  String namaCust;
  String ordernumber;
  String workKey;
  String startTime;
  String endTime;
  String dateTime;
  String productname;
  String technology;
  String packagetype;
  String custid;
  String customerServiceInstanceId;

  factory WorkOrder.fromDocument(DocumentSnapshot document) {
    return WorkOrder(
      status: document["status"] ?? "",
      id: document["id"] ?? "",
      type: document["type"] ?? "",
      namaCust: document["nama_cust"] ?? "",
      ordernumber: document["ordernumber"] ?? "",
      workKey: document["workKey"] ?? "",
      startTime: document["startTime"] ?? "",
      endTime: document["endTime"] ?? "",
      dateTime: document["dateTime"] ?? "",
      technology: document["technology"] ?? "",
      productname: document["productname"] ?? "",
      custid: document["custid"] ?? "",
      customerServiceInstanceId: document["customerServiceInstanceID"] ?? "",
    );
  }

  factory WorkOrder.fromJson(Map<String, dynamic> json) => WorkOrder(
    status: json["status"],
    id: json["id"],
    type: json["type"],
    namaCust: json["nama_cust"],
    ordernumber: json["ordernumber"],
    workKey: json["workKey"],
    startTime: json["startTime"],
    endTime: json["endTime"],
    dateTime: json["dateTime"],
    technology: json["technology"],
    productname: json["productname"],
    packagetype: json["packagetype"],
    custid: json["custid"],
    customerServiceInstanceId: json["customerServiceInstanceID"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "id": id,
    "type": type,
    "nama_cust": namaCust,
    "ordernumber": ordernumber,
    "workKey": workKey,
    "startTime": startTime,
    "endTime": endTime,
    "dateTime": dateTime,
    "productname": productname,
    "packagetype": packagetype,
    "custid": custid,
    "customerServiceInstanceID": customerServiceInstanceId,
  };
}
