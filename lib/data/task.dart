import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

Task taskFromJson(String str) => Task.fromJson(json.decode(str));

String taskToJson(Task data) => json.encode(data.toJson());

class Task {
  Task({
    this.name,
    this.alias,
    this.done,
    this.taskDoneId,
    this.workId,
  });

  String name;
  String alias;
  bool done;
  dynamic taskDoneId;
  String workId;

  factory Task.fromDocument(DocumentSnapshot document) {
    return Task(
        name: document["name"],
        alias: document["name"],
        done: document["done"],
        taskDoneId: document["taskDoneId"],
        workId: document["workId"]
    );
  }

  factory Task.fromJson(Map<String, dynamic> json) => Task(
    name: json["name"],
    alias: json["name"],
    done: json["done"],
    taskDoneId: json["taskDoneId"],
    workId: json["workId"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "alias": alias,
    "done": done,
    "taskDoneId": taskDoneId,
    "workId": workId,
  };
}
